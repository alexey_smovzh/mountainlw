package com.fasthamster.mountainlw.android;

import android.content.Context;
import android.content.SharedPreferences;

import com.fasthamster.mountainlw.Constants;

/**
 * Created by alex on 30.09.16.
 */

public class SettingsHandler {

    Context context;
    SharedPreferences settings;
    SharedPreferences.Editor editor;

    private int sceneChange;
    private int dayLength;
    private boolean parallax;
    private boolean bonfire;
    private boolean clouds;

    private boolean sceneChangeChanged;
    private boolean dayLengthChanged;
    private boolean parallaxChanged;
    private boolean bonfireChanged;
    private boolean cloudsChanged;


    public SettingsHandler(Context context) {

        this.context = context;
        load();

    }

    public void load() {

        settings = context.getSharedPreferences(Constants.PREFS_NAME, 0);

        sceneChange = settings.getInt(Constants.SCENE_CHANGE_ALIAS, Constants.SCENE_CHANGE_DEFAULT);
        dayLength = settings.getInt(Constants.DAY_LENGTH_ALIAS, Constants.DAY_LENGTH_DEFAULT);
        parallax = settings.getBoolean(Constants.PARALLAX_ALIAS, Constants.PARALLAX_DEFAULT);
        bonfire = settings.getBoolean(Constants.BONFIRE_ALIAS, Constants.BONFIRE_DEFAULT);
        clouds = settings.getBoolean(Constants.CLOUDS_ALIAS, Constants.CLOUDS_DEFAULT);

    }

    public void save() {

        editor = settings.edit();

        editor.putInt(Constants.SCENE_CHANGE_ALIAS, sceneChange);
        editor.putInt(Constants.DAY_LENGTH_ALIAS, dayLength);
        editor.putBoolean(Constants.PARALLAX_ALIAS, parallax);
        editor.putBoolean(Constants.BONFIRE_ALIAS, bonfire);
        editor.putBoolean(Constants.CLOUDS_ALIAS, clouds);

        editor.commit();

        clearStates();
    }

    private void clearStates() {

        sceneChangeChanged = false;
        dayLengthChanged = false;
        parallaxChanged = false;
        bonfireChanged = false;
        cloudsChanged = false;

    }

    public boolean isPreferencesChanged() {

        if(sceneChangeChanged == true) return true;
        if(dayLengthChanged == true) return true;
        if(parallaxChanged == true) return true;
        if(bonfireChanged == true) return true;
        if(cloudsChanged == true) return true;

        return false;

    }

    public int getSceneChange() { return  sceneChange; }
    public int getDayLength() { return dayLength; }
    public boolean getParallax() { return parallax; }
    public boolean getBonfire() { return bonfire; }
    public boolean getClouds() { return clouds; }

    public void setSceneChange(int s) {
        this.sceneChange = s;
        this.sceneChangeChanged = true;
    }
    public void setDayLength(int l) {
        this.dayLength = l;
        this.dayLengthChanged = true;
    }
    public void setParallax(boolean p) {
        this.parallax = p;
        this.parallaxChanged = true;
    }
    public void setBonfire(boolean b) {
        this.bonfire = b;
        this.bonfireChanged = true;
    }
    public void setClouds(boolean c) {
        this.clouds = c;
        this.cloudsChanged = true;
    }

}

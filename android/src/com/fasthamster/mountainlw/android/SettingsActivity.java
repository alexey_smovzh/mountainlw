package com.fasthamster.mountainlw.android;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.fasthamster.mountainlw.Constants;
import com.fasthamster.mountainlw.MountainLW;


/**
 * Created by alex on 30.09.16.
 */


public class SettingsActivity extends Activity {

    private Spinner sceneChange;
    private SeekBar dayLength;
    private TextView resultDayLength;
    private CheckBox parallax;
    private CheckBox bonfire;
    private CheckBox clouds;
    private Button rateMe;


    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // Layouts
        setContentView(R.layout.settings);

        sceneChange = (Spinner)findViewById(R.id.sp_scene_change);
        dayLength = (SeekBar)findViewById(R.id.sb_scene_length);
        resultDayLength = (TextView)findViewById(R.id.tv_scene_length_result);
        parallax = (CheckBox)findViewById(R.id.chk_parallax);
        bonfire = (CheckBox)findViewById(R.id.chk_bonfire);
        clouds = (CheckBox)findViewById(R.id.chk_clouds);
        rateMe = (Button)findViewById(R.id.btn_rate);

        // Listeners
        // Scene change spinner listener
        sceneChange.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                LiveWallpaper.settings.setSceneChange(position);
                setBonfireState(position);

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });
        // Scene length listener
        dayLength.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if(progress < Constants.MIN_DAY_LENGTH)     // control min value no less than 20
                    progress = Constants.MIN_DAY_LENGTH;

                LiveWallpaper.settings.setDayLength(progress);
                resultDayLength.setText(convertToMinutes(progress));

            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });
        // Parallax checkbox listener
        parallax.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                LiveWallpaper.settings.setParallax(isChecked);
            }
        });
        // Bonfire checkbox listener
        bonfire.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                LiveWallpaper.settings.setBonfire(isChecked);
            }
        });
        // Clouds checkbox listener
        clouds.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                LiveWallpaper.settings.setClouds(isChecked);
            }
        });

        // RateMe button listener
        rateMe.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {

                Intent market = new Intent(Intent.ACTION_VIEW);
                market.setData(Uri.parse("market://details?id=" + LiveWallpaper.PAID_PACKAGE));

                Intent website = new Intent(Intent.ACTION_VIEW);
                website.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + LiveWallpaper.PAID_PACKAGE));

                try {
                    startActivity(market);
                } catch (ActivityNotFoundException e) {
                    startActivity(website);
                }
            }
        });

        sceneChange.setSelection(LiveWallpaper.settings.getSceneChange());
        dayLength.setProgress(LiveWallpaper.settings.getDayLength());
        parallax.setChecked(LiveWallpaper.settings.getParallax());
        bonfire.setChecked(LiveWallpaper.settings.getBonfire());
        clouds.setChecked(LiveWallpaper.settings.getClouds());

        setBonfireState(LiveWallpaper.settings.getSceneChange());
    }

    private String convertToMinutes(int seconds) {

        if(seconds < 60) {
            return String.format("0:%02d", seconds);
        } else {
            return String.format("%01d:%02d", seconds / 60, seconds % 60);
        }
    }

    // In always day scene bonfire not running, so disable it checkbox
    private void setBonfireState(int scene) {
        switch (scene) {
            case Constants.DAY_AND_NIGHT:
                bonfire.setEnabled(true);
                dayLength.setEnabled(true);
                break;
            case Constants.ALWAYS_DAY:
                bonfire.setEnabled(false);
                dayLength.setEnabled(false);
                break;
            case Constants.ALWAYS_NIGHT:
                bonfire.setEnabled(true);
                dayLength.setEnabled(false);
                break;
        }
    }

    @Override
    protected void onPause() {

        if(LiveWallpaper.settings.isPreferencesChanged() == true) {
            LiveWallpaper.settings.save();
            MountainLW.setPreferencesChanged(true);
        }

        super.onPause();

    }

    @Override
    protected void onDestroy() {

        super.onDestroy();

    }
}

package com.fasthamster.mountainlw.android;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.Surface;
import android.view.WindowManager;

import com.fasthamster.mountainlw.Constants;
import com.fasthamster.mountainlw.DeviceOrientation;

import java.util.List;


/**
 * Created by alex on 27.01.16.
 */
public class OrientationHandler implements SensorEventListener, DeviceOrientation {

    private boolean exist;

    private SensorManager manager;
    private WindowManager window;
    private List<Sensor> sensors;
    Context context;

    private static float[] mRotationMatrix = new float[16];
    private static float[] mRemappedMatrix = new float[16];
    private static float[] values = new float[2];


    public OrientationHandler(Context context) {

        this.context = context;

        manager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        window = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        exist = registerOrientationListener();

    }

    private boolean registerOrientationListener() {

        sensors = manager.getSensorList(Sensor.TYPE_ROTATION_VECTOR);

        if(sensors.size() != 0) {
            manager.registerListener(this, sensors.get(0), SensorManager.SENSOR_DELAY_UI);
            return true;
        } else {
            return false;
        }
    }

    private float[] lowPass(float[] input, float[] output) {

        if(output == null) return input;

        output[0] = output[0] + Constants.ALPHA * (input[0] - output[0]);
        output[1] = output[1] + Constants.ALPHA * (input[1] - output[1]);
        output[2] = output[2] + Constants.ALPHA * (input[2] - output[2]);

        return output;
    }

    public boolean isAvailable() {
        return this.exist;
    }

    @Override
    public float[] getOrientation() {

        return values;

    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    private float[] sensorVals = new float[3];
    private float[] matrixValues = new float[3];
    @Override
    public void onSensorChanged(SensorEvent event) {

        sensorVals = lowPass(event.values, sensorVals);

        if(sensorVals != null) {

            SensorManager.getRotationMatrixFromVector(mRotationMatrix, sensorVals);
            int rot = window.getDefaultDisplay().getRotation();

            switch (rot) {
                case Surface.ROTATION_90:
                    SensorManager.remapCoordinateSystem(mRotationMatrix, SensorManager.AXIS_Y,
                            SensorManager.AXIS_MINUS_X, mRemappedMatrix);
                    break;
                case Surface.ROTATION_180:
                    SensorManager.remapCoordinateSystem(mRotationMatrix, SensorManager.AXIS_MINUS_X,
                            SensorManager.AXIS_MINUS_Y, mRemappedMatrix);
                    break;
                case Surface.ROTATION_270:
                    SensorManager.remapCoordinateSystem(mRotationMatrix, SensorManager.AXIS_MINUS_Y,
                            SensorManager.AXIS_X, mRemappedMatrix);
                    break;
            }

            SensorManager.getOrientation(mRotationMatrix, matrixValues);
            values[0] = (float) Math.toDegrees(matrixValues[1]);    // pitch
            values[1] = (float) Math.toDegrees(matrixValues[2]);    // roll

        }
    }

    @Override
    public void pause() {
        manager.unregisterListener(this);
    }

    @Override
    public void resume() {
        exist = registerOrientationListener();
    }
}


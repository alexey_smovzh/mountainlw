package com.fasthamster.mountainlw.android;

import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.AndroidLiveWallpaperService;
import com.badlogic.gdx.backends.android.AndroidWallpaperListener;
import com.fasthamster.mountainlw.DeviceOrientation;
import com.fasthamster.mountainlw.MountainLW;


public class LiveWallpaper extends AndroidLiveWallpaperService {

	private MountainLWListener listener;
    private OrientationHandler handler;
	public static SettingsHandler settings;

	public static final String PAID_PACKAGE = "com.fasthamster.mountainlw.android";


	@Override
	public void onCreateApplication() {

		super.onCreateApplication();

		// Initialize gyroscope
        handler = new OrientationHandler(getApplicationContext());

		// Settings staff
		settings = new SettingsHandler(getApplicationContext());

		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		config.useCompass = false;
		config.useWakelock = false;
		config.getTouchEventsForLiveWallpaper = false;
		config.useAccelerometer = handler.isAvailable();

		listener = new MountainLWListener(handler);

		initialize(listener, config);

	}

	@Override
	public Engine onCreateEngine() {
		return new AndroidWallpaperEngine(){
			@Override
			public void onPause(){
				super.onPause();
				listener.pause();
				handler.pause();
			}

			@Override
			public void onResume(){
				super.onResume();
				handler.resume();
			}
		};
	}

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

	public static class MountainLWListener extends MountainLW implements AndroidWallpaperListener {


        public MountainLWListener(DeviceOrientation orientation) {
            super(orientation);
        }

        @Override
		public void offsetChange(float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset) {

		}

		@Override
		public void previewStateChange(boolean isPreview) {

		}
	}

}

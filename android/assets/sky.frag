uniform sampler2D u_diffuseTexture;
uniform float u_starsAlphaFactor;
uniform vec2 u_resolution;
uniform vec4 u_gradientColor0;
uniform vec4 u_gradientColor1;
uniform vec4 u_gradientColor2;
uniform vec4 u_gradientColor3;
uniform float u_gradientFactor;

varying MED vec2 v_diffuseUV;

void main() {

    vec2 position = gl_FragCoord.xy / u_resolution.xy;

    vec4 bottom = mix(u_gradientColor0, u_gradientColor2, u_gradientFactor);
    vec4 top = mix(u_gradientColor1, u_gradientColor3, u_gradientFactor);
    vec4 color = mix(top, bottom, position.y);

    vec4 texColor = texture2D(u_diffuseTexture, v_diffuseUV);

    if(texColor.a >= 0.01)
        color = mix(color, texColor, texColor.a * u_starsAlphaFactor);

    gl_FragColor = color;
}
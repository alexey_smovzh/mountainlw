uniform mat4 u_worldTrans;
uniform mat4 u_projViewTrans;

attribute vec3 a_position;
attribute vec2 a_texCoord0;

varying vec2 v_diffuseUV;

void main() {

    v_diffuseUV = a_texCoord0;
    gl_Position = u_projViewTrans * u_worldTrans * vec4(a_position, 1.0);

}
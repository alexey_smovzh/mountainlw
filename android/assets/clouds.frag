varying MED vec2 v_diffuseUV;

uniform sampler2D u_diffuseTexture;
uniform float u_nightFactor;

const vec3 nightColor = vec3(0.25, 0.31, 0.39);

void main() {

    vec4 diffuse = texture2D(u_diffuseTexture, v_diffuseUV);

    if(diffuse.a <= 0.01)
        discard;

    gl_FragColor = vec4(mix(diffuse.rgb, nightColor, u_nightFactor), diffuse.a);
}
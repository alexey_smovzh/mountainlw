#define NIGHT vec3(0.74, 0.65, 0.56)
#define WEIGHT vec3(0.2125, 0.7154, 0.0721)

uniform float u_nightFactor;
uniform sampler2D u_diffuseTexture;
uniform float u_shininess;

varying MED vec2 v_diffuseUV;

void main() {

    vec4 diffuse = texture2D(u_diffuseTexture, v_diffuseUV);

    if(diffuse.a <= 0.01)
        discard;

    // Dirty hack to disable bonfire light plane2 texture coloring in fragment shader
    float factor = u_nightFactor;
    if(u_shininess == 1.0)
        factor = 0.0;

    if(factor > 0.0) {
        vec3 saturation = vec3(mix(diffuse.rgb, vec3(dot(diffuse.rgb, WEIGHT)), u_nightFactor * 0.6));
        vec3 night = vec3(saturation * (vec3(1.0) - NIGHT * u_nightFactor));
        diffuse = vec4(night, diffuse.a);
    }

    gl_FragColor = diffuse;
}
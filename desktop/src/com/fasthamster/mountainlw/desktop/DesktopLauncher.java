package com.fasthamster.mountainlw.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.fasthamster.mountainlw.MountainLW;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
//        config.width = 540;
//        config.height = 960;
        config.width=1024;
        config.height=700;
		new LwjglApplication(new MountainLW(new DesktopOrientationHandler()), config);
	}

    // Dummy class for device orientation on PC
    private static class DesktopOrientationHandler implements com.fasthamster.mountainlw.DeviceOrientation {

        @Override
        public boolean isAvailable() { return false; }

        @Override
        public float[] getOrientation() { return new float[0]; }

        @Override
        public void pause() {}

        @Override
        public void resume() {}
    }
}

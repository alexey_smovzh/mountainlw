package com.fasthamster.mountainlw;


import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import java.util.Random;


public class MountainLW implements ApplicationListener {

    public final static Random rand = new Random();

	private boolean paused;

    private static DeviceOrientation orientation;
    private CameraRotation cameraRotation;
    private TouchscreenProcessor touchscreenProcessor;

	private ModelBatch modelBatch;
	private PerspectiveCamera camera;

	private Mountains mountains;
    private Sky sky;
    private Bonfire bonfire;
    private Clouds clouds;

    private float currentNightFactor = 0f;              // wallpaper starts from day
    private float targetNightFactor;

    private Vector2 resolution = new Vector2();
    public static Vector3 cameraPosition;
    public static Vector3 cameraLookAt;
    public static Vector3 cameraDirection;
    public static Vector3 cameraUp;

    private static boolean preferencesChanged;
    private int sceneType;
    private int interval;                                // scene duration in frames
    private boolean availableParallax;
    private boolean availableBonfire;
    private boolean availableClouds;

    // Scene states
    private boolean isPrevSceneDay = true;              // wallpaper start from day
    private boolean isCurrentSceneDay = true;
    private boolean isBonfire = false;


    // Constructor
    public MountainLW(DeviceOrientation orientation) {
        MountainLW.orientation = orientation;
    }

	@Override
	public void create () {

        Gdx.app.setLogLevel(Application.LOG_NONE);

        // Model batch with custom comparator
        modelBatch = new ModelBatch(new MountainRenderableSorter());

        // Load models and shaders
        sky = new Sky(modelBatch, new Vector2(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
        bonfire = new Bonfire(modelBatch);
        mountains = new Mountains(modelBatch, bonfire);
        clouds = new Clouds(modelBatch);

        setupPreferences();

        paused = false;

	}

	private void setupCam(int w, int h) {

        if(camera == null) {
            camera = new PerspectiveCamera(50f, w, h);

            // If available use orientation vector, else use touchscreen drag events to rotate camera
            if (orientation.isAvailable() == true) {
                cameraRotation = new CameraRotation(orientation, camera);
            } else {
                touchscreenProcessor = new TouchscreenProcessor(camera);
                Gdx.input.setInputProcessor(touchscreenProcessor);
            }

        } else {
            camera.viewportWidth = w;
            camera.viewportHeight = h;
        }

        camera.position.set(cameraPosition);
        camera.lookAt(cameraLookAt);

        camera.near = 0.1f;
        camera.far = 100f;
        camera.update();

        // Save initial camera params for CameraRotation be able to return camera to initial position
        cameraUp = camera.up.cpy();
        cameraDirection = camera.direction.cpy();

    }

    private void setupPreferences() {

        // Load shared preferences
        Preferences preferences = Gdx.app.getPreferences(Constants.PREFS_NAME);
        setSceneChange(preferences.getInteger(Constants.SCENE_CHANGE_ALIAS, Constants.SCENE_CHANGE_DEFAULT));
        setInterval(preferences.getInteger(Constants.DAY_LENGTH_ALIAS, Constants.DAY_LENGTH_DEFAULT));
        setParallax(preferences.getBoolean(Constants.PARALLAX_ALIAS, Constants.PARALLAX_DEFAULT));
        setBonfire(preferences.getBoolean(Constants.BONFIRE_ALIAS, Constants.BONFIRE_DEFAULT));
        setClouds(preferences.getBoolean(Constants.CLOUDS_ALIAS, Constants.CLOUDS_DEFAULT));

        preferencesChanged = false;
    }

    // Setters
    public static void setPreferencesChanged(boolean state) { preferencesChanged = state    ; }

    private void setSceneChange(int s) {
        this.sceneType = s;

        // User set scene type, if it differ from current type, change to new immediately
        if(sceneType == Constants.ALWAYS_NIGHT && isCurrentSceneDay == true)
            changeScene();

        if(sceneType == Constants.ALWAYS_DAY && isCurrentSceneDay == false)
            changeScene();

    }

    // Multiply user defined value by fps to archive total frames count of scene duration
    private void setInterval(int i) {

        if(i < Constants.SCENE_CHANGE_DURATION) i = Constants.SCENE_CHANGE_DURATION;            // Scene length can't be smaller than scene change
        interval = i * Constants.FPS;
    }

    // If enabled enabled allow parallax
    // if orientation vector is not available set and unset touchscreenprocessor accordingly
    // if orientation vector available register and unregister orientation vector listener
    private void setParallax(boolean p) {

        if(p == false) {
            if (orientation.isAvailable() == false) {
                Gdx.input.setInputProcessor(null);
            } else {
                orientation.pause();
            }
        } else {
            if (orientation.isAvailable() == false) {
                Gdx.input.setInputProcessor(touchscreenProcessor);
            } else {
                orientation.resume();
            }
        }

        availableParallax = p;
    }

    private void setBonfire(boolean b) {

        this.availableBonfire = b;          // availability

    }

    private void setClouds(boolean c) {

        this.availableClouds = c;

    }

	@Override
	public void render () {

		if (!paused) {

			Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

			// Update cycle
            update();                   // main update

            sky.update(isCurrentSceneDay, currentNightFactor);
            mountains.update(isCurrentSceneDay, currentNightFactor);

            modelBatch.begin(camera);

            Gdx.gl.glEnable(GL20.GL_BLEND);
            Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

			// Render models
            sky.render();
            bonfire.render();
            clouds.render(currentNightFactor);
			mountains.render();

			modelBatch.end();

            // Sleep for battery consumption
			sleep(Constants.FPS);

		}
	}

    int i = 0;
    private void update() {

        // Handle orientation
        if(availableParallax == true)
            if(orientation.isAvailable() == true) {
                cameraRotation.update();
            } else {
                touchscreenProcessor.update();
            }

        // Scene control
        if(i >= interval) {
            changeScene();
            i = 0;
        }
        i++;

        if(currentNightFactor != targetNightFactor) {                   // if they not equal change

            if(targetNightFactor == 1f) {                               // make night
                currentNightFactor += Constants.STEP;
                if(currentNightFactor > targetNightFactor)              // if we reach the target make factors equal and stop calculations
                    currentNightFactor = targetNightFactor;
            } else {
                currentNightFactor -= Constants.STEP;
                if(currentNightFactor < targetNightFactor)
                    currentNightFactor = targetNightFactor;
            }
        }
    }

	// Sleep for power consumption
	private long diff, start = System.currentTimeMillis();
	private void sleep(int fps) {

		diff = System.currentTimeMillis() - start;
		long targetDelay = 1000 / fps;
		if(diff < targetDelay) {
			try {
				Thread.sleep(targetDelay - diff);
			} catch (InterruptedException e) {	}
		}
		start = System.currentTimeMillis();
	}

	@Override
	public void resize(int w, int h) {
        // resolution was actually changed
        if(resolution.x != (float)w) {

            resolution.set((float)w, (float)h);

            if (w < h) {                         // Portrait
                cameraPosition = Constants.CAM_PORTRAIT_POSITION;
                cameraLookAt = Constants.CAM_PORTRAIT_LOOKAT;
                mountains.setDeviceOrientation(Constants.ORIENTATION_PORTRAIT); // Pass current orientation to moves mountain planes
                clouds.setDeviceOrientation(Constants.ORIENTATION_PORTRAIT);
            } else {
                cameraPosition = Constants.CAM_LANDSCAPE_POSITION;
                cameraLookAt = Constants.CAM_LANDSCAPE_LOOKAT;
                mountains.setDeviceOrientation(Constants.ORIENTATION_LANDSCAPE);
                clouds.setDeviceOrientation(Constants.ORIENTATION_LANDSCAPE);
            }

            setupCam(w, h);

            // Pass current resolution to sky for gradient shader calculation
            sky.setResolution(resolution);
        }
    }

    // Scene controls staff -------------------------------
    private void setDay() {

        if(isCurrentSceneDay == true) return;           // already day do nothing

        if(isPrevSceneDay == false) {
            currentNightFactor = 1f;
            targetNightFactor = 0f;
        }

        mountains.makeDay();
        sky.makeDay();

        isCurrentSceneDay = true;
        isPrevSceneDay = true;
    }

    private void setNight() {

        if(availableBonfire == true) {
            isBonfire = rand.nextBoolean();                                                         // random bonfire start
        } else {
            isBonfire = false;
        }

        // already night change only bonfire
        if(isCurrentSceneDay == false) {
            mountains.makeNightAlwaysNight(isBonfire);
            sky.makeNightAlwaysNight();
            return;
        }

        if(isPrevSceneDay == true) {
            currentNightFactor = 0f;
            targetNightFactor = 1f;
        }

        mountains.makeNight(isBonfire);
        sky.makeNight(rand.nextBoolean() ? Constants.NIGHT_MILKYWAY : Constants.NIGHT_STARS);   // random choose texture

        isCurrentSceneDay = false;
        isPrevSceneDay = false;
    }

    private void changeScene() {

       // Clouds starts doesn't depend at time of day
        if(availableClouds == true) {

            if(clouds.isRunning() == true)                    // Stop clouds
                clouds.stop();

            if(rand.nextBoolean() == true)         // Randomly start new clouds
                clouds.start();

        }

        // Setup new scene
        switch (sceneType) {

            case Constants.DAY_AND_NIGHT:
                if(isPrevSceneDay == true) {
                    setNight();
                } else {
                    setDay();
                }
                break;

            case Constants.ALWAYS_DAY:
                setDay();
                break;

            case Constants.ALWAYS_NIGHT:
                setNight();
                break;
        }
    }
    // Ends scene controls staff ---------------------------

	@Override
	public void pause() {

		paused = true;

	}

	@Override
	public void resume() {

        if(preferencesChanged == true)
            setupPreferences();

        paused = false;

	}

	@Override
	public void dispose() {

		mountains.dispose();
        bonfire.dispose();
        clouds.dispose();
        sky.dispose();

		modelBatch.dispose();

	}
}

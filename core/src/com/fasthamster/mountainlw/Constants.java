package com.fasthamster.mountainlw;

import com.badlogic.gdx.math.Vector3;

/**
 * Created by alex on 11.01.16.
 */
public class Constants {

    // Scene
    public final static int FPS = 30;
    public final static int SCENE_CHANGE_DURATION = 10;      // time for which scene changed in seconds

    public final static float STEP = 1f / (FPS * SCENE_CHANGE_DURATION);
    // RenderableSorter don't sort renderables which center Y position is negative
    // this constant used for make all instances Y position positive respectively to its real Z position
    public final static float CENTER_DIFF = 2f;

    public static final boolean DAY = true;
    public static final boolean NIGHT = false;

    public static final int DAY_AND_NIGHT = 0;
    public static final int ALWAYS_DAY = 1;
    public static final int ALWAYS_NIGHT = 2;
// TODO: add follow current time of day
// TODO: add follow current time of day in opposite order (if currently day - display night and over)
// set scene type to always_day/always_night accordingly to current time of day
    // Shared Preferences stuff
    public static final String PREFS_NAME = "settings";

    public static final String SCENE_CHANGE_ALIAS = "scenechange";
    public static final String DAY_LENGTH_ALIAS = "daylength";
    public static final String PARALLAX_ALIAS = "parallax";
    public static final String BONFIRE_ALIAS = "bonfire";
    public static final String CLOUDS_ALIAS = "clouds";

    public static final int SCENE_CHANGE_DEFAULT = DAY_AND_NIGHT;
    public static final int DAY_LENGTH_DEFAULT = 60;                            // scene duration in seconds
    public static final int MIN_DAY_LENGTH = 20;
    public static final boolean PARALLAX_DEFAULT = true;
    public static final boolean BONFIRE_DEFAULT = true;
    public static final boolean CLOUDS_DEFAULT = true;


    // Accelerometer
    public final static float ALPHA = 0.1f;                       // Accelerometer threshold
    public final static float DELAY = 0.6f;                       // Delay in seconds to return camera rotation to initial position


    // Camera
    public static final Vector3 CAM_PORTRAIT_POSITION = new Vector3(0f, 0.8f, 3.4f);
    public static final Vector3 CAM_PORTRAIT_LOOKAT = new Vector3(0f, 0.8f, 0f);
    public static final Vector3 CAM_LANDSCAPE_POSITION = new Vector3(0f, 0.1f, 1.6f);
    public static final Vector3 CAM_LANDSCAPE_LOOKAT = new Vector3(0f, 0.2f, 0f);


    // State constants
    public static final byte NIGHT_MILKYWAY = 1;
    public static final byte NIGHT_STARS = 2;

    public static final float STARS_ROTATION_SPEED = 0.04f;

    public static final boolean WIND_RIGHT_2_LEFT = true;                      // wind directions
    public static final boolean WIND_LEFT_2_RIGHT = false;

    public static final boolean ORIENTATION_PORTRAIT = true;
    public static final boolean ORIENTATION_LANDSCAPE = false;


}

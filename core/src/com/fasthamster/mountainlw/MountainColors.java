package com.fasthamster.mountainlw;


import com.badlogic.gdx.graphics.Color;


/**
 * Created by alex on 11.01.16.
 */
public class MountainColors {

    
    // holder for sky colors
    static class ColorsSet {

        Color Color0;
        Color Color1;

        public ColorsSet(Color Color0, Color Color1) {

            this.Color0 = Color0;
            this.Color1 = Color1;

        }
    }

    private static final ColorsSet[] nightSkyColors = new ColorsSet[] {
            new ColorsSet(new Color(.004f, .016f, .046f, 1f), new Color(.005f, .102f, .334f, 1f)),
            new ColorsSet(new Color(.006f, .008f, .014f, 1f), new Color(.006f, .092f, .220f, 1f)),
            new ColorsSet(new Color(.003f, .014f, .036f, 1f), new Color(.027f, .097f, .22f, 1f))
    };

    private static final ColorsSet[] daySkyColors = new ColorsSet[] {
            new ColorsSet(new Color(0f, .027f, .127f, 1f), new Color(.09f, .6f, 1f, 1f)),
            new ColorsSet(new Color(0f, .161f, .246f, 1f), new Color(.002f, .622f, 1f, 1f)),
            new ColorsSet(new Color(.01f, .184f, .367f, 1f), new Color(.434f, .584f, .8f, 1f))
    };

    // Getters
    public static ColorsSet getRandomDayColors() {

        return daySkyColors[MountainLW.rand.nextInt(daySkyColors.length)];

    }

    public static ColorsSet getRandomNightColors() {

        return nightSkyColors[MountainLW.rand.nextInt(nightSkyColors.length)];

    }

    public static ColorsSet getNightColor(int position) {

        return nightSkyColors[position];

    }

}

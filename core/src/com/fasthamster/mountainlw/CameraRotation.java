package com.fasthamster.mountainlw;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by alex on 31.01.16.
 */
public class CameraRotation {

    private Camera camera;
    private final DeviceOrientation orientation;
    private float[] initialDeviceOrientation = new float[2];

    private float anglePitch;
    private float angleRoll;

    private static final Vector3 ROTATE_AROUND = Vector3.Zero;
    private static final float FACTOR = 0.2f;                                   // rotation speed factor
    private static final float MAX_ANGLE = 6f;



    public CameraRotation(DeviceOrientation orientation, PerspectiveCamera camera) {

        this.orientation = orientation;
        this.camera = camera;

        System.arraycopy(orientation.getOrientation(), 0, this.initialDeviceOrientation, 0, 2);

    }

    float rX, rY;
    private void process(float deltaX, float deltaY) {

        rX = deltaX * FACTOR;
        if(anglePitch < MAX_ANGLE) {            // pitch up
            pitch(rX);
        } else if(anglePitch > -MAX_ANGLE) {    // pitch down
            pitch(rX);
        }

        rY = deltaY * FACTOR;
        if(angleRoll < MAX_ANGLE) {
            roll(rY);
        } else if(angleRoll > -MAX_ANGLE) {
            roll(rY);
        }

        camera.update();

    }

    private void pitch(float angle) {
        camera.rotateAround(ROTATE_AROUND, Vector3.X, angle);
        anglePitch += angle;
    }

    private void roll(float angle) {
        camera.rotateAround(ROTATE_AROUND, Vector3.Y, angle);
        angleRoll += angle;
    }

    private void setCameraToInitialPosition() {

        camera.position.set(MountainLW.cameraPosition);
        camera.direction.set(MountainLW.cameraDirection);
        camera.up.set(MountainLW.cameraUp);

    }

    private void returnToInitialPosition() {

        if(anglePitch > 0f) {
            pitch(-Constants.ALPHA);
        }
        if(anglePitch < 0f) {
            pitch(Constants.ALPHA);
        }
        if(angleRoll > 0f) {
            roll(-Constants.ALPHA);
        }
        if(angleRoll < 0f) {
            roll(Constants.ALPHA);
        }

        if (Math.abs(anglePitch) + Math.abs(angleRoll) <= Constants.ALPHA) {
            anglePitch = 0f;
            angleRoll = 0f;
            setCameraToInitialPosition();
        }

        camera.update();

    }

    float diffPitch, diffRoll;
    public void update() {

        float[] o = orientation.getOrientation();

        diffPitch = initialDeviceOrientation[0] - o[0];
        diffRoll = initialDeviceOrientation[1] - o[1];

        // Save current device orientation
        initialDeviceOrientation[0] = o[0];
        initialDeviceOrientation[1] = o[1];

        process(diffPitch, diffRoll);

        // Return to initial camera position
        if(Math.abs(anglePitch) > Constants.ALPHA || Math.abs(angleRoll) > Constants.ALPHA)
            returnToInitialPosition();

    }
}
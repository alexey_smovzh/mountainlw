package com.fasthamster.mountainlw;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;


/**
 * Created by alex on 11.01.16.
 */
public class Sky {

    private ModelInstance sky;
    private ModelBatch batch;
    private SkyShader shader;

    private Texture milkywayTex;
    private Texture starsTex;
    private TextureAttribute milkyway;
    private TextureAttribute stars;

    private static final String MILKYWAY_TEXTURE = "starfield.png";
    private static final String STARS_TEXTURE = "stars.png";

    private MountainColors.ColorsSet skyColors;
    private MountainColors.ColorsSet targetSkyColors;

    private float angle = 0f;
    private byte prevTex = Constants.NIGHT_STARS;
    private boolean skyRotate = false;
    private boolean makingChanges = false;



    // Constructor
    public Sky(ModelBatch batch, Vector2 resolution) {

        this.batch = batch;

        setupModel();
        setupShader(resolution);

    }

    private void setupModel() {

        ModelBuilder builder = new ModelBuilder();
        Model model = builder.createSphere(8f, 8f, 8f, 16, 16,
                                           new Material(),
                                           VertexAttributes.Usage.Position | VertexAttributes.Usage.TextureCoordinates);

        model.meshParts.get(0).center.set(0f, Constants.CENTER_DIFF * 2, 0f);
        sky = new ModelInstance(model);

        // Load textures at once
        milkywayTex = new Texture(Gdx.files.internal(MILKYWAY_TEXTURE));
        starsTex = new Texture(Gdx.files.internal(STARS_TEXTURE));
        milkywayTex.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        starsTex.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        milkyway = TextureAttribute.createDiffuse(milkywayTex);
        stars = TextureAttribute.createDiffuse(starsTex);

        // Set stars texture
        sky.materials.get(0).set(TextureAttribute.createDiffuse(starsTex));

    }

    private void setupShader(Vector2 resolution) {

        Renderable r = new Renderable();
        sky.getRenderable(r);
        shader = new SkyShader(r);
        shader.init();

        shader.setResolution(resolution);

        // Dummy launch
        skyColors = MountainColors.getRandomDayColors();
        shader.setColor0(skyColors.Color0);
        shader.setColor1(skyColors.Color1);
        shader.setColor2(skyColors.Color0);
        shader.setColor3(skyColors.Color1);
        shader.setFactor(0f);
        shader.setGradientFactor(0f);

    }

    // If device orientation changed set new resolution
    public void setResolution(Vector2 r) {

        shader.setResolution(r);

    }

    private void interpolateSkyColors(MountainColors.ColorsSet current, MountainColors.ColorsSet target, float factor) {

        shader.setColor0(current.Color0);
        shader.setColor1(current.Color1);
        shader.setColor2(target.Color0);
        shader.setColor3(target.Color1);
        shader.setGradientFactor(factor);

    }

    // Check if texture type changed from prev run and load new
    private void setStarsTexture(byte type) {

        if(prevTex != type) {
            switch(type) {
                case Constants.NIGHT_MILKYWAY:
                    sky.materials.get(0).set(milkyway);
                    break;
                case Constants.NIGHT_STARS:
                    sky.materials.get(0).set(stars);
                    break;
            }

            // Random Z angle between 45 and 135 degrees and Rotate to angle from prev night
            // Change it only if texture changed
            sky.transform.setToRotation(Vector3.Z, MountainLW.rand.nextFloat() * (135f - 45f) + 45f);
            angle = (angle > 360f) ? 0f: angle;
            sky.transform.rotate(Vector3.Y, angle);
        }

        prevTex = type;

        skyRotate = true;

    }

    // Controls
    public void makeNight(byte type) {

        if(makingChanges == false) {
            makingChanges = true;
            targetSkyColors = MountainColors.getRandomNightColors();
            setStarsTexture(type);
        }
    }

    public void makeNightAlwaysNight() {

        if(makingChanges == false) {
            makingChanges = true;
            targetSkyColors = MountainColors.getNightColor(2);
            setStarsTexture(Constants.NIGHT_MILKYWAY);
        }
    }

    public void makeDay() {

        if(makingChanges == false) {
            makingChanges = true;

            targetSkyColors = MountainColors.getRandomDayColors();
        }
    }

    public void update(boolean isDay, float factor) {

        // Changing sky color and stars visibility
        if(makingChanges == true) {                                 // Something need to update, check it

            shader.setFactor(factor);

            if(isDay == Constants.NIGHT) {
                interpolateSkyColors(skyColors, targetSkyColors, factor);

                if(factor == 1f) {                                  // got night
                    makingChanges = false;
                    skyColors = targetSkyColors;
                }

            } else {
                interpolateSkyColors(skyColors, targetSkyColors, 1f - factor);

                if(factor == 0f) {                                  // got day
                    makingChanges = false;
                    skyRotate = false;
                    skyColors = targetSkyColors;
                }
            }
        }

        // Rotate stars
        if(skyRotate) {
            sky.transform.rotate(Vector3.Y, Constants.STARS_ROTATION_SPEED);
            angle += Constants.STARS_ROTATION_SPEED;
        }
    }

    public void render() {

        batch.render(sky, shader);

    }

    public void dispose() {

        if(milkywayTex != null) milkywayTex.dispose();
        if(starsTex != null) starsTex.dispose();
        if(shader != null) shader.dispose();

        sky = null;

    }
}

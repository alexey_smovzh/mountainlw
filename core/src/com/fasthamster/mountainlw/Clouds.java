package com.fasthamster.mountainlw;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;



/**
 * Created by alex on 21.01.16.
 */
public class Clouds {

    private ModelBatch batch;
    private ShaderProgram program;
    private Mesh mesh;

    private Array<Particle> particles = new Array<Particle>();

    private Texture texture;
    private Material material;
    private boolean running;
    private boolean stopping = false;

    private static float currentTime;
    private Vector3 velocity;                                                   // Move vector and speed
    private Vector3 startPoint;
    private float particlesPerSec;
    private float lifeTime;
    private boolean deviceOrientation;

    private static final float SIZE = 1f;
    private static final String TEXTURE = "clouds.png";
    private static final Vector2 UV_TEXTURE = new Vector2(4f, 4f);              // Count of texture rows and cols
    private static final Vector3 START_POINT_LEFT = new Vector3(-2f, 2f, 0f);
    private static final Vector3 START_POINT_RIGHT = new Vector3(2f, 2f, 0f);
    private static final Vector3 START_POINT_LEFT_LANDSCAPE = new Vector3(-2f, 1.4f, 0f);
    private static final Vector3 START_POINT_RIGHT_LANDSCAPE = new Vector3(2f, 1.4f, 0f);
    private static final float DEPTH = 2f;
    private static final float DEPTH_FACTOR = 0.2f;                             // Factor for make deeper cloud down and smaller
    private static final float MAX_CLOUDS_PER_SEC = 0.5f;
    private static final float SPEED = 0.08f;


    // Pre-run calc
    private static final int TEXTURE_REGIONS_COUNT = (int)(UV_TEXTURE.x * UV_TEXTURE.y);


    //Particle data holder class
    public static class Particle {
        public Renderable renderable;
        public CloudsShader shader;
        public float startTime;
        public float lifeTime;

        public Particle(Renderable renderable, CloudsShader shader,
                        float startTime, float lifeTime) {
            this.renderable = renderable;
            this.shader = shader;
            this.startTime = startTime;
            this.lifeTime = lifeTime;
        }
    }

    public Clouds(ModelBatch batch) {

        this.batch = batch;

        setupShaderProgram();
        setupMesh();
        setupMaterial();

    }
    // Setup shaderprogram at once
    private void setupShaderProgram() {

        String vert = Gdx.files.internal("clouds.vert").readString();
        String frag = Gdx.files.internal("clouds.frag").readString();

        program = new ShaderProgram(Shaders.GLSL_PREFIX + vert, Shaders.GLSL_PREFIX + frag);

    }

    // Generate particle mesh
    private void setupMesh() {

        mesh = new Mesh(true,
                4, 6,
                new VertexAttribute(VertexAttributes.Usage.Position, 3, "a_position"),
                new VertexAttribute(VertexAttributes.Usage.TextureCoordinates, 2, "a_texCoord0"));

        mesh.setVertices(new float[]{           // Vertices + UVs
                                    SIZE, -SIZE, 0f, 0f, 1f,
                                    -SIZE, -SIZE, 0f, 1f, 1f,
                                    -SIZE, SIZE, 0f, 1f, 0f,
                                    SIZE, SIZE, 0f, 0f, 0f});

        mesh.setIndices(new short[]{0, 1, 2, 2, 3, 0});

    }

    // Load texture at once
    private void setupMaterial() {

        texture = new Texture(Gdx.files.internal(TEXTURE));
        texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        material = new Material(new BlendingAttribute(GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA, 1f),
                                TextureAttribute.createDiffuse(texture));

    }

    int i = 0;
    // Emit particles
    private void emit() {

        // Take next texture coord
        i++;
        if(i == TEXTURE_REGIONS_COUNT) i = 0;

        // Start time
        float startTime = getCurrentTime();

        // Create particle
        Renderable particle = createRenderable();

        CloudsShader shader = new CloudsShader(particle, program);
        shader.setUV(calcUVMapping(i));
        shader.setStartTime(startTime);
        shader.setVelocity(velocity);
        shader.init();

        particle.shader = shader;

        particles.add(new Particle(particle, shader, startTime, lifeTime));

    }

    private Renderable createRenderable() {

        Renderable renderable = new Renderable();
        renderable.meshPart.primitiveType = GL20.GL_TRIANGLES;
        renderable.meshPart.offset = 0;
        renderable.meshPart.size = mesh.getNumIndices();
        renderable.material = material;
        renderable.meshPart.mesh = mesh;

        // Little depth variance
        float depth = startPoint.z - MountainLW.rand.nextFloat() * DEPTH;
        float height = startPoint.y + depth * DEPTH_FACTOR;
        float scale = 1f + depth * DEPTH_FACTOR;

        renderable.worldTransform.translate(new Vector3(startPoint.x, height, depth));
        renderable.worldTransform.scl(scale);

        // clouds center interval from 1f to 3f (2f - center of mountain planes)
        // real Z position interval from 0f to -2f
        // so for RenderableSorter i need to convert ranges by this rule: -2 = 3, 0 = 1
        renderable.meshPart.center.set(0f, Math.abs(depth) + 1f, 0f);

        return renderable;
    }


    // Calculates texture coordinates offsets for vertex shader
    private Vector2 calcUVMapping(int num) {

        int row, column;

        column = (num / (int)UV_TEXTURE.y);

        if(num > (int)UV_TEXTURE.y) {
            row = num - (int)UV_TEXTURE.y;
        } else {
            row = num;
        }

        return new Vector2(1f / UV_TEXTURE.x * column, 1f / UV_TEXTURE.y * row);
    }

    // Setup clouds params
    private void setupParams() {

        // Generate random clouds params
        particlesPerSec = MountainLW.rand.nextFloat() * MAX_CLOUDS_PER_SEC;
        lifeTime = START_POINT_RIGHT.x * 2f / SPEED + 20f;

        // Decide clouds directions, speed and start point
        if(MountainLW.rand.nextBoolean() == Constants.WIND_RIGHT_2_LEFT) {

            if(deviceOrientation == Constants.ORIENTATION_PORTRAIT) {
                startPoint = START_POINT_RIGHT;
            } else {
                startPoint = START_POINT_RIGHT_LANDSCAPE;
            }

            this.velocity = new Vector3(-SPEED, 0f, 0f);

        } else {

            if(deviceOrientation == Constants.ORIENTATION_PORTRAIT) {
                startPoint = START_POINT_LEFT;
            } else {
                startPoint = START_POINT_LEFT_LANDSCAPE;
            }

            this.velocity = new Vector3(SPEED, 0f, 0f);
        }
    }

    public void setDeviceOrientation(boolean orientation) {

        this.deviceOrientation = orientation;

    }

    // Controls
    public void start() {

        setupParams();

        currentTime = 0f;

        running = true;
    }

    public void stop() {

        stopping = true;                            // if call stopping clouds
                                                    // cancel emitting new clouds
        if(particles.size == 0) {                   // but wait until remaining leave the frustrum
            running = false;
            stopping = false;
            particles.clear();
        }
    }

    public boolean isRunning() {

        return running;

    }

    public static float getCurrentTime() { return currentTime; }

    // Render model
    int delta = 0;
    public void render(float factor) {

        if (running == true) {

            // Update model
            if (stopping == false) {
                if (delta >= Constants.FPS / particlesPerSec) {
                    emit();
                    delta = 0;
                } else {
                    delta++;
                }
            }

            for (int i = 0; i < particles.size; i++) {
                Particle p = particles.get(i);

                if (getCurrentTime() - p.startTime > p.lifeTime) {           // if particle exceeds life time remove it
                    particles.removeIndex(i);
                    p.renderable.shader.dispose();
                } else {
                    p.shader.setNightFactor(factor);
                    batch.render(p.renderable);
                }
            }

            currentTime += Gdx.graphics.getDeltaTime();
        }
    }

    public void dispose() {

        particles.clear();
        if(material != null) material.clear();
        if(texture != null) texture.dispose();
        if(program != null) program.dispose();

    }
}

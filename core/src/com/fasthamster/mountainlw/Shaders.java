package com.fasthamster.mountainlw;

/**
 * Created by alex on 31.01.16.
 */
public class Shaders {

    // Shaders header
    public final static String GLSL_PREFIX = "#ifdef GL_ES\n" +
            "    #define LOWP lowp\n" +
            "    #define MED mediump\n" +
            "    #define HIGH highp\n" +
            "    precision mediump float;\n" +
            "#else\n" +
            "    #define MED\n" +
            "    #define LOWP\n" +
            "    #define HIGH\n" +
            "#endif\n";

}
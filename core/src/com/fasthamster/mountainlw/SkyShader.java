package com.fasthamster.mountainlw;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.Attributes;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.shaders.BaseShader;
import com.badlogic.gdx.graphics.g3d.shaders.DefaultShader;
import com.badlogic.gdx.graphics.g3d.utils.RenderContext;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;



/**
 * Created by alex on 12.01.16.
 */
public class SkyShader extends BaseShader {


    private final int u_projViewTrans = register(new Uniform("u_projViewTrans"), DefaultShader.Setters.projViewTrans);
    private final int u_worldTrans = register(new Uniform("u_worldTrans"), DefaultShader.Setters.worldTrans);
    private final int u_diffuseTexture = register(new Uniform("u_diffuseTexture", TextureAttribute.Diffuse), DefaultShader.Setters.diffuseTexture);

    private final int u_resolution = register(new Uniform("u_resolution"));
    private final int u_gradientColor0 = register(new Uniform("u_gradientColor0"));
    private final int u_gradientColor1 = register(new Uniform("u_gradientColor1"));
    private final int u_gradientColor2 = register(new Uniform("u_gradientColor2"));
    private final int u_gradientColor3 = register(new Uniform("u_gradientColor3"));
    private final int u_starsAlphaFactor = register(new Uniform("u_starsAlphaFactor"));
    private final int u_gradientFactor = register(new Uniform("u_gradientFactor"));

    private Vector2 resolution;
    private Color color0;
    private Color color1;
    private Color color2;
    private Color color3;
    private float factor;
    private float gradient;


    private Renderable renderable;


    // Constructor
    public SkyShader(final Renderable renderable) {

        String vert = Gdx.files.internal("sky.vert").readString();
        String frag = Gdx.files.internal("sky.frag").readString();

        this.program = new ShaderProgram(Shaders.GLSL_PREFIX + vert, Shaders.GLSL_PREFIX + frag);
        this.renderable = renderable;

    }

    // Setters
    public void setResolution(Vector2 r) { this.resolution = r; }
    public void setColor0(Color c0) { this.color0 = c0; }
    public void setColor1(Color c1) { this.color1 = c1; }
    public void setColor2(Color c2) { this.color2 = c2; }
    public void setColor3(Color c3) { this.color3 = c3; }
    public void setFactor(float f) { this.factor = f; }
    public void setGradientFactor(float g) { this.gradient = g; }

    @Override
    public void init() {

        final ShaderProgram program = this.program;
        this.program = null;
        init(program, renderable);
        renderable = null;

    }

    @Override
    public void begin(final Camera camera, final RenderContext context) {

        super.begin(camera, context);

        set(u_resolution, resolution);
        set(u_gradientColor0, color0);
        set(u_gradientColor1, color1);
        set(u_gradientColor2, color2);
        set(u_gradientColor3, color3);
        set(u_starsAlphaFactor, factor);
        set(u_gradientFactor, gradient);

    }

    @Override
    public void render(Renderable renderable, Attributes combinedAttributes) {

        super.render(renderable, combinedAttributes);

    }

    @Override
    public void end() {

        super.end();

    }

    @Override
    public int compareTo(Shader other) {
        return 0;
    }

    @Override
    public boolean canRender(Renderable renderable) {

        return true;

    }
}

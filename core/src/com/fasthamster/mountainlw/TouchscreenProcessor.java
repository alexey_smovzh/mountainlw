package com.fasthamster.mountainlw;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by alex on 07.11.16.
 */

/*       on tap save tap coordinates
         calculate delta between original coordinate and current
         rotate camera for delta angle
         on tap off wait and move camera to initial position
 */

public class TouchscreenProcessor extends InputAdapter {

    private PerspectiveCamera camera;
    private float startX, startY;

    private float anglePitch;
    private float angleRoll;

    private static final Vector3 ROTATE_AROUND = Vector3.Zero;
    private static final float FACTOR = 4f;                                     // rotation speed factor
    private static final float MAX_ANGLE = 8f;


    // Constructor
    public TouchscreenProcessor(PerspectiveCamera camera) {

        this.camera = camera;

    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        startX = screenX;
        startY = screenY;

        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    float deltaX, deltaY;
    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {

        deltaX = (startX - screenX) / Gdx.graphics.getWidth();
        deltaY = (startY - screenY) / Gdx.graphics.getHeight();

        startX = screenX;
        startY = screenY;

        process(deltaX, deltaY);

        return false;
    }

    private void process(float deltaX, float deltaY) {

        if(Math.abs(anglePitch) <= MAX_ANGLE)
            pitch(deltaY * FACTOR);

        if(Math.abs(angleRoll) <= MAX_ANGLE)
            roll(deltaX * FACTOR);

        camera.update();
    }

    private void setCameraToInitialPosition() {

        camera.position.set(MountainLW.cameraPosition);
        camera.direction.set(MountainLW.cameraDirection);
        camera.up.set(MountainLW.cameraUp);

    }

    private void returnToInitialPosition() {

        if(anglePitch > 0f) {
            pitch(-Constants.ALPHA);
        }
        if(anglePitch < 0f) {
            pitch(Constants.ALPHA);
        }
        if(angleRoll > 0f) {
            roll(-Constants.ALPHA);
        }
        if(angleRoll < 0f) {
            roll(Constants.ALPHA);
        }

        if (Math.abs(anglePitch) + Math.abs(angleRoll) <= Constants.ALPHA) {
            anglePitch = 0f;
            angleRoll = 0f;
            setCameraToInitialPosition();
        }

        camera.update();

    }

    private void pitch(float angle) {
        camera.rotateAround(ROTATE_AROUND, Vector3.X, angle);
        anglePitch += angle;
    }

    private void roll(float angle) {
        camera.rotateAround(ROTATE_AROUND, Vector3.Y, angle);
        angleRoll += angle;
    }

    public void update() {

        // Return to initial camera position
        if(Math.abs(anglePitch) > Constants.ALPHA || Math.abs(angleRoll) > Constants.ALPHA)
            returnToInitialPosition();

    }
}

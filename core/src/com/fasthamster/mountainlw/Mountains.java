package com.fasthamster.mountainlw;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.attributes.FloatAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.loader.G3dModelLoader;
import com.badlogic.gdx.graphics.g3d.model.Node;
import com.badlogic.gdx.utils.UBJsonReader;



/**
 * Created by alex on 24.12.15.
 */
public class Mountains {

    private ModelInstance instance;
    private ModelBatch batch;
    private MountainsShader shader;
    private Bonfire bonfire;

    private Texture bonfireTex;
    private Texture plane2Tex;

    private boolean isPlanesPortrait;
    private boolean makingChanges = false;
    private boolean bonfireLight;


    public Mountains(ModelBatch batch, Bonfire bonfire) {

        this.batch = batch;
        this.bonfire = bonfire;

        setupModel();
        setupShader();

    }

    private void setupModel() {

        UBJsonReader jsonReader = new UBJsonReader();
        G3dModelLoader modelLoader = new G3dModelLoader(jsonReader);

        Model model = modelLoader.loadModel(Gdx.files.internal("scene.g3db"));

        instance = new ModelInstance(model);
        for(Material m : instance.materials)                        // Dirty hack to disable bonfire light plane2 texture coloring in fragment shader
            m.set(FloatAttribute.createShininess(0f));

        for(Node n : instance.nodes)
            n.parts.get(0).meshPart.center.set(0f, Constants.CENTER_DIFF, 0f);


        // Load textures at once
        bonfireTex = new Texture(Gdx.files.internal("plane2_nf.png"));
        plane2Tex = new Texture(Gdx.files.internal("plane2.png"));

        isPlanesPortrait = Constants.ORIENTATION_PORTRAIT;          // planes just loaded and no moved

    }

    public void setDeviceOrientation(boolean orientation) {

        if(orientation != isPlanesPortrait) {

            if(orientation == Constants.ORIENTATION_PORTRAIT) {
                movePanelsPortrait();
            } else {
                movePanelsLandscape();
            }
        }
    }

    private void movePanelsLandscape() {

        instance.nodes.get(0).globalTransform.translate(0f, 0f, -0.3f);     // plane 5
        instance.nodes.get(1).globalTransform.translate(0f, 0f, -0.18f);
        instance.nodes.get(2).globalTransform.translate(0f, 0f, -0.16f);
//        instance.nodes.get(3).globalTransform.translate(0f, 0f, 0f);
        instance.nodes.get(4).globalTransform.translate(0f, 0f, 0.1f);      // plane 1

        isPlanesPortrait = Constants.ORIENTATION_LANDSCAPE;
    }

    private void movePanelsPortrait() {

        instance.nodes.get(0).globalTransform.translate(0f, 0f, 0.3f);     // plane 5
        instance.nodes.get(1).globalTransform.translate(0f, 0f, 0.18f);
        instance.nodes.get(2).globalTransform.translate(0f, 0f, 0.16f);
//        instance.nodes.get(3).globalTransform.translate(0f, 0f, 0f);
        instance.nodes.get(4).globalTransform.translate(0f, 0f, -0.1f);      // plane 1

        isPlanesPortrait = Constants.ORIENTATION_PORTRAIT;
    }

    private void setupShader() {

        Renderable r = new Renderable();
        instance.getRenderable(r);
        shader = new MountainsShader(r);
        shader.init();

    }

    // Controls
    public void makeNight(boolean bonfire) {

        if(makingChanges == false) {
            makingChanges = true;
            bonfireLight = bonfire;
        }
    }

    public void makeNightAlwaysNight(boolean bonfire) {

        if(bonfireLight == bonfire) return;                 // If bonfire new state equals prev - do nothing

        stopBonfire();
        makeNight(bonfire);
    }

    public void makeDay() {

        if(makingChanges == false) {
            makingChanges = true;

            if(bonfireLight == true) {                       // Remove bonfire light texture
                stopBonfire();
            }
        }
    }

    private void stopBonfire() {

        removeBonfireTexture();
        bonfire.stop();                                     // Stop bonfire
        bonfireLight = false;

    }

    public void setupBonfireTexture() {

        instance.materials.get(1).set(TextureAttribute.createDiffuse(bonfireTex));
        instance.materials.get(1).set(FloatAttribute.createShininess(1f));

    }

    public void removeBonfireTexture() {

        instance.materials.get(1).set(TextureAttribute.createDiffuse(plane2Tex));
        instance.materials.get(1).set(FloatAttribute.createShininess(0f));

    }

    // Render model
    public void render() {

        batch.render(instance, shader);

    }

    // Update model
    public void update(boolean isDay, float factor) {

        if(makingChanges == true) {                                 // Something need to update, check it

            shader.setNightFactor(factor);

            if(isDay == Constants.NIGHT && factor == 1f) {          // got night

                if(bonfireLight == true) {
                    setupBonfireTexture();
                    bonfire.start();
                }
                makingChanges = false;

            } else if (isDay == Constants.DAY && factor == 0f) {     // got day

                makingChanges = false;

            }
        }
    }

    public void dispose() {

        if(bonfireTex != null) bonfireTex.dispose();
        if(plane2Tex != null) plane2Tex.dispose();
        if(shader != null) shader.dispose();

    }
}

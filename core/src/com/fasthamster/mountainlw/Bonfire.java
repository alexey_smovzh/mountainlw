package com.fasthamster.mountainlw;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;


/**
 * Created by alex on 21.01.16.
 */


public class Bonfire {

    private ModelBatch batch;
    private ShaderProgram program;
    private Mesh mesh;

    private Array<Particle> particles = new Array<Particle>();

    private static float currentTime;
    private Texture texture;
    private Material material;
    private boolean running;

    private static final float SIZE = 0.07f;
    private static final String TEXTURE = "flame.png";
    private static final Vector2 UV_TEXTURE = new Vector2(4f, 4f);          // Count of texture rows and cols
    private static final Vector3 START_POINT = new Vector3(-0.36f, -0.4f, 0.2f);
    private static final Vector3 VELOCITY = new Vector3(0f, 0.04f, 0f);      // Move vector and speed
    private static final float LIFE_TIME = 3f;
    private static final int MAX_PARTICLES = 30;

    // Pre-run calc
    private static final int TEXTURE_REGIONS_COUNT = (int)(UV_TEXTURE.x * UV_TEXTURE.y);
    private static final float PARTICLES_INTERVAL = LIFE_TIME * VELOCITY.y / MAX_PARTICLES;
    private static final float STEP = VELOCITY.y / Constants.FPS;


    // Particle data holder class
    public class Particle {
        public Renderable renderable;
        public BonfireShader shader;
        public float startTime;
        public float lifeTime;

        public Particle(Renderable renderable, BonfireShader shader, float startTime, float lifeTime) {
            this.renderable = renderable;
            this.shader = shader;
            this.startTime = startTime;
            this.lifeTime = lifeTime;
        }
    }


    public Bonfire(ModelBatch batch) {

        this.batch = batch;

        setupShaderProgram();
        setupMesh();
        setupMaterial();

    }

    // Setup shaderprogram at once
    private void setupShaderProgram() {

        String vert = Gdx.files.internal("bonfire.vert").readString();
        String frag = Gdx.files.internal("bonfire.frag").readString();

        program = new ShaderProgram(Shaders.GLSL_PREFIX + vert, Shaders.GLSL_PREFIX + frag);

    }

    // Generate particle mesh
    private void setupMesh() {

        mesh = new Mesh(true,
                4, 6,
                new VertexAttribute(VertexAttributes.Usage.Position, 3, "a_position"),
                new VertexAttribute(VertexAttributes.Usage.TextureCoordinates, 2, "a_texCoord0"));

        mesh.setVertices(new float[]{           // Vertices + UVs
                                    SIZE, -SIZE, 0f, 0f, 1f,
                                    -SIZE, -SIZE, 0f, 1f, 1f,
                                    -SIZE, SIZE, 0f, 1f, 0f,
                                    SIZE, SIZE, 0f, 0f, 0f});

        mesh.setIndices(new short[]{0, 1, 2, 2, 3, 0});

    }

    // Load texture at once
    private void setupMaterial() {

        texture = new Texture(Gdx.files.internal(TEXTURE));
        texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        material = new Material(new BlendingAttribute(GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA, 1f),
                                TextureAttribute.createDiffuse(texture));

    }

    public static float getCurrentTime() { return currentTime; }

    int i = 0;
    // Emit particles
    private void emit() {

        // Take next flame texture coord
        i++;
        if(i == TEXTURE_REGIONS_COUNT) i = 0;

        // Start time
        float startTime = getCurrentTime();

        // Create particle
        Renderable particle = createRenderable();

        BonfireShader shader = new BonfireShader(particle, program);
        shader.setUV(calcUVMapping(i));
        shader.setStartTime(startTime);
        shader.setVelocity(VELOCITY);
        shader.init();

        particle.shader = shader;                                               // Assign shader

        particles.add(new Particle(particle, shader, startTime, LIFE_TIME));

    }

    private Renderable createRenderable() {

        Renderable renderable = new Renderable();
        renderable.meshPart.primitiveType = GL20.GL_TRIANGLES;
        renderable.meshPart.offset = 0;
        renderable.meshPart.size = mesh.getNumIndices();
        renderable.material = material;
        renderable.meshPart.mesh = mesh;

        renderable.worldTransform.translate(START_POINT);

        renderable.meshPart.center.set(0f, Constants.CENTER_DIFF - START_POINT.z, 0f);      // for RenderableSorter

        return renderable;
    }

    // Calculates texture coordinates offsets for vertex shader
    private Vector2 calcUVMapping(int num) {

        int row, column;

        column = (num / (int)UV_TEXTURE.x);                         // column num, starts from 0

        if(num > (int)UV_TEXTURE.x) {
            row = num - (column * (int)UV_TEXTURE.x);
        } else {
            row = num;
        }

        return new Vector2(1f / UV_TEXTURE.x * column, 1f / UV_TEXTURE.y * row);
    }

    // Controls
    public void start() {

        currentTime = 0f;

        running = true;

    }

    public void stop() {

        running = false;
        particles.clear();

    }

    float delta = 0;
    float time = 0;
    // Render model
    public void render() {

        if(running == true) {

            // Update
            if(particles.size < MAX_PARTICLES) {
                if (delta > PARTICLES_INTERVAL) {
                    emit();
                    delta = 0f;
                }

                delta += STEP;
            }

            for(int i = 0; i < particles.size; i++) {
                Particle p = particles.get(i);

                if(getCurrentTime() - p.startTime > p.lifeTime) {
                    time = getCurrentTime();
                    p.startTime = time;
                    p.shader.setStartTime(time);
                }

                batch.render(p.renderable);
            }

            currentTime += Gdx.graphics.getDeltaTime();

        }
    }

    public void dispose() {

        particles.clear();
        if(material != null) material.clear();
        if(texture != null) texture.dispose();
        if(program != null) program.dispose();

    }
}

package com.fasthamster.mountainlw;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g3d.Attributes;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.shaders.BaseShader;
import com.badlogic.gdx.graphics.g3d.shaders.DefaultShader;
import com.badlogic.gdx.graphics.g3d.utils.RenderContext;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;


/**
 * Created by alex on 10.01.16.
 */
public class MountainsShader extends BaseShader {


    private final int u_projViewTrans = register(new Uniform("u_projViewTrans"), DefaultShader.Setters.projViewTrans);
    private final int u_worldTrans = register(new Uniform("u_worldTrans"), DefaultShader.Setters.worldTrans);
    private final int u_diffuseTexture = register(new Uniform("u_diffuseTexture", TextureAttribute.Diffuse), DefaultShader.Setters.diffuseTexture);
    private final int u_shininess = register(new Uniform("u_shininess"), DefaultShader.Setters.shininess);

    private final int u_nightFactor = register(new Uniform("u_nightFactor"));

    private float nightFactor;

    private Renderable renderable;


    // Constructor
    public MountainsShader(final Renderable renderable) {

        String vert = Gdx.files.internal("mountain.vert").readString();
        String frag = Gdx.files.internal("mountain.frag").readString();

        this.program = new ShaderProgram(Shaders.GLSL_PREFIX + vert, Shaders.GLSL_PREFIX + frag);
        this.renderable = renderable;

    }

    // Setters
    public void setNightFactor(float f) { this.nightFactor = f; }

    @Override
    public void init() {

        final ShaderProgram program = this.program;
        this.program = null;
        init(program, renderable);
        renderable = null;

    }

    @Override
    public void begin(final Camera camera, final RenderContext context) {

        super.begin(camera, context);

        set(u_nightFactor, nightFactor);

    }

    @Override
    public void render(Renderable renderable, Attributes combinedAttributes) {

        super.render(renderable, combinedAttributes);

    }

    @Override
    public void end() {

        super.end();

    }

    @Override
    public int compareTo(Shader other) {
        return 0;
    }

    @Override
    public boolean canRender(Renderable renderable) {

        return true;

    }
}

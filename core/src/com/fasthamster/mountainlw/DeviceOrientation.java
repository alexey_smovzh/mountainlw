package com.fasthamster.mountainlw;

/**
 * Created by alex on 27.01.16.
 */
public interface DeviceOrientation {

    boolean isAvailable();
    float[] getOrientation();
    void pause();
    void resume();

}

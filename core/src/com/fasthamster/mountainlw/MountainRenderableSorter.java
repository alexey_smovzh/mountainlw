package com.fasthamster.mountainlw;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.utils.RenderableSorter;
import com.badlogic.gdx.utils.Array;

import java.util.Comparator;

/**
 * Created by alex on 27.10.16.
 */

public class MountainRenderableSorter implements RenderableSorter, Comparator<Renderable> {


    @Override
    public void sort (final Camera camera, final Array<Renderable> renderables) {
        renderables.sort(this);
    }

    @Override
    public int compare (final Renderable o1, final Renderable o2) {

        if(o1.meshPart.center.y == o2.meshPart.center.y) return 0;
        if(o1.meshPart.center.y < o2.meshPart.center.y) {
            return 1;
        } else {
            return -1;
        }

//        return o1.meshPart.center.y > o2.meshPart.center.y ? 1 : 0;

    }
}